	<EE_SpaceUnit Name="Hutts">

		<Text_ID>TEXT_DALEK_SOLDIER</Text_ID>
		<Encyclopedia_Good_Against> SW_TIE_Interceptor FV_Type9 Starviper_Fighter </Encyclopedia_Good_Against>
		<Encyclopedia_Vulnerable_To> SW_Corellian_Corvette FV_Constitution Firestar </Encyclopedia_Vulnerable_To>
		<Encyclopedia_Text>TEXT_TOOLTIP_DALEK_SOLDIER</Encyclopedia_Text>
		<Encyclopedia_Unit_Class>TEXT_ENCYCLOPEDIA_CLASS_FIGHTER</Encyclopedia_Unit_Class>
		<Space_Model_Name>dalek.ALO</Space_Model_Name>
		<Scale_Factor>0.05</Scale_Factor>
		<Select_Box_Scale>70</Select_Box_Scale>
		<Select_Box_Z_Adjust>-1</Select_Box_Z_Adjust>

		<Mouse_Collide_Override_Sphere_Radius> 50.0 </Mouse_Collide_Override_Sphere_Radius>

		<Ranged_Target_Z_Adjust>0</Ranged_Target_Z_Adjust>
		<Dense_FOW_Reveal_Range_Multiplier>0.2</Dense_FOW_Reveal_Range_Multiplier>
		<Mass>0.985</Mass>
		<Max_Speed>10</Max_Speed>
		<Min_Speed>0</Min_Speed>
		<Max_Rate_Of_Turn>5.0</Max_Rate_Of_Turn>
		<Max_Lift>4</Max_Lift>
		<Max_Thrust>3.0</Max_Thrust>
		<Max_Rate_Of_Roll>5.0</Max_Rate_Of_Roll>
		<Bank_Turn_Angle>40</Bank_Turn_Angle>
		<Hyperspace>no</Hyperspace>
		<Hyperspace_Speed>0</Hyperspace_Speed>
		<Begin_Turn_Towards_Distance>800.0</Begin_Turn_Towards_Distance>
		<Fires_Forward>no</Fires_Forward>
		<Turret_Rotate_Extent_Degrees>20</Turret_Rotate_Extent_Degrees>
		<Turret_Elevate_Extent_Degrees>40</Turret_Elevate_Extent_Degrees>
		<Affiliation>B5</Affiliation>
		<Required_Ground_Base_Level>0</Required_Ground_Base_Level>
		<Required_Star_Base_Level>1</Required_Star_Base_Level>
		<Shield_Points>0</Shield_Points>
		<Tactical_Health>100</Tactical_Health>
		<Shield_Refresh_Rate>0</Shield_Refresh_Rate>
		<Energy_Capacity>500</Energy_Capacity>
		<Energy_Refresh_Rate>300</Energy_Refresh_Rate>
		<Ship_Class>fighter</Ship_Class>
		<Armor_Type> Armor_gen_fighter </Armor_Type>
		<Formation_Priority>1</Formation_Priority>
		<Is_Escort>Yes</Is_Escort>
		<Is_Bomber>no</Is_Bomber>
		<Political_Control>0</Political_Control>
		<Squadron_Capacity>0</Squadron_Capacity>
		<Number_per_Squadron>3</Number_per_Squadron>
		<Build_Cost_Credits>120</Build_Cost_Credits>
		<Build_Time_Seconds>15</Build_Time_Seconds>
		<Size_Value>8</Size_Value>
		<SpaceBehavior>HUNT, FIGHTER_LOCOMOTOR, SELECTABLE, TARGETING, WEAPON, POWERED, HIDE_WHEN_FOGGED, AMBIENT_SFX, DAMAGE_TRACKING, NEBULA </SpaceBehavior>
		<Collidable_By_Projectile_Living>Yes</Collidable_By_Projectile_Living>
		<Damage>3</Damage>

		<HardPoints>
			Dalek_Soldier
		</HardPoints>

		<Land_FOW_Reveal_Range>130.0</Land_FOW_Reveal_Range>
		<Space_FOW_Reveal_Range>2000.0</Space_FOW_Reveal_Range>
		<Targeting_Max_Attack_Distance>500.0</Targeting_Max_Attack_Distance>
		<Death_Explosions>Small_Explosion_Space_Empire</Death_Explosions>
		<Death_SFXEvent_Start_Die>Unit_Todesgleiter_Death_SFX</Death_SFXEvent_Start_Die>
		<Asteroid_Damage_Hit_Particles>EE_Small_Damage_Space</Asteroid_Damage_Hit_Particles>

		<SFXEvent_Fire> Unit_Death_Glider_Fire </SFXEvent_Fire>
		<SFXEvent_Select> Unit_Select_Todesgleiter </SFXEvent_Select>
		<SFXEvent_Move> Unit_Move_Todesgleiter </SFXEvent_Move>
		<SFXEvent_Fleet_Move>Unit_Fleet_Move_Todesgleiter</SFXEvent_Fleet_Move>
		<SFXEvent_Attack> Unit_Attack_Todesgleiter </SFXEvent_Attack>
		<SFXEvent_Guard> Unit_Guard_Todesgleiter </SFXEvent_Guard>
		<SFXEvent_Move_Into_Asteroid_Field> Unit_Asteroids_Todesgleiter </SFXEvent_Move_Into_Asteroid_Field>
		<SFXEvent_Move_Into_Nebula> Unit_Nebula_Todesgleiter </SFXEvent_Move_Into_Nebula>

		<SFXEvent_Health_Low_Warning> Unit_Health_Low_Todesgleiter</SFXEvent_Health_Low_Warning>
		<SFXEvent_Health_Critical_Warning> Unit_Health_Critical_Todesgleiter </SFXEvent_Health_Critical_Warning>
		<SFXEvent_Enemy_Damaged_Health_Low_Warning> Unit_Enemy_Health_Low_Todesgleiter </SFXEvent_Enemy_Damaged_Health_Low_Warning>
		<SFXEvent_Enemy_Damaged_Health_Critical_Warning> Unit_Enemy_Health_Critical_Todesgleiter </SFXEvent_Enemy_Damaged_Health_Critical_Warning>

		<SFXEvent_Attack_Hardpoint> HARD_POINT_WEAPON_LASER, Unit_HP_LASER_Todesgleiter </SFXEvent_Attack_Hardpoint>
		<SFXEvent_Attack_Hardpoint> HARD_POINT_WEAPON_MISSILE, Unit_HP_MISSILE_Todesgleiter </SFXEvent_Attack_Hardpoint>
		<SFXEvent_Attack_Hardpoint> HARD_POINT_WEAPON_TORPEDO, Unit_HP_TORP_Todesgleiter </SFXEvent_Attack_Hardpoint>
		<SFXEvent_Attack_Hardpoint> HARD_POINT_WEAPON_ION_CANNON, Unit_HP_ION_Todesgleiter </SFXEvent_Attack_Hardpoint>
		<SFXEvent_Attack_Hardpoint> HARD_POINT_SHIELD_GENERATOR, Unit_HP_SHIELDS_Todesgleiter </SFXEvent_Attack_Hardpoint>
		<SFXEvent_Attack_Hardpoint> HARD_POINT_ENGINE, Unit_HP_ENGINES_Todesgleiter </SFXEvent_Attack_Hardpoint>
		<SFXEvent_Attack_Hardpoint> HARD_POINT_GRAVITY_WELL, Unit_HP_GRAV_Todesgleiter </SFXEvent_Attack_Hardpoint>

		<!-- Test: Each TIE fighter will play a 3D ambient "flyby" sound every X to Y seconds -->
		<!-- Note: To invoke SFXEvent_Ambient, AMBIENT_SFX behavior needs to be added to the behavior list -->
		<SFXEvent_Ambient_Moving> Unit_Todesgleiter_Fly_By </SFXEvent_Ambient_Moving>
		<SFXEvent_Ambient_Moving_Min_Delay_Seconds> 5 </SFXEvent_Ambient_Moving_Min_Delay_Seconds>
		<SFXEvent_Ambient_Moving_Max_Delay_Seconds> 10 </SFXEvent_Ambient_Moving_Max_Delay_Seconds>
		<SFXEvent_Engine_Cinematic_Focus_Loop> Unit_Todesgleiter_Cinematic_Engine_Loop </SFXEvent_Engine_Cinematic_Focus_Loop>

		<CategoryMask> Fighter | AntiBomber</CategoryMask>
		<Property_Flags> SmallShip </Property_Flags>
		<Icon_Name>I_BUTTON_dalek_soldier.tga</Icon_Name>
		<Victory_Relevant>Yes</Victory_Relevant>
                <Blob_Shadow_Below_Detail_Level>2</Blob_Shadow_Below_Detail_Level>
		<Blob_Shadow_Scale>40.0, 40.0</Blob_Shadow_Scale>
		<Blob_Shadow_Material_Name>Generic_Shadow</Blob_Shadow_Material_Name>
		<Is_Visible_On_Radar>Yes</Is_Visible_On_Radar>

		<Custom_Footprint_Radius> 15.0 </Custom_Footprint_Radius>
		<MovementClass> Space </MovementClass>
		<FormationOrder>1</FormationOrder>
		<Out_Of_Combat_Defense_Adjustment>-1.0</Out_Of_Combat_Defense_Adjustment>
		<Minimum_Follow_Distance>60.0</Minimum_Follow_Distance>
		<Create_Team>Yes</Create_Team>
		<AI_Combat_Power>35</AI_Combat_Power>
		<Collision_Box_Modifier>2.0</Collision_Box_Modifier>
		<Targeting_Priority_Set>Fighter</Targeting_Priority_Set>
		<Targeting_Stickiness_Time_Threshold>5.0</Targeting_Stickiness_Time_Threshold>

		<No_Colorization_Color> 75, 75, 75, 255 </No_Colorization_Color>
		
		<!-- Non-Hero unit abilities description -->
		<Unit_Abilities_Data SubObjectList="Yes">
			<!-- Primary ability -->
			<Unit_Ability>
				<Type>HUNT</Type>
			</Unit_Ability>
		</Unit_Abilities_Data>
		
		<Spin_Away_On_Death>Yes</Spin_Away_On_Death>
		<Spin_Away_On_Death_Chance>0.4</Spin_Away_On_Death_Chance>
		<Spin_Away_On_Death_Time>2.0f</Spin_Away_On_Death_Time>
		<Spin_Away_On_Death_Explosion>Small_Explosion_Space_Empire</Spin_Away_On_Death_Explosion>
		<Spin_Away_On_Death_SFXEvent_Start_Die>Unit_Todesgleiter_Spinning_By</Spin_Away_On_Death_SFXEvent_Start_Die>
		<Remove_Upon_Death>true</Remove_Upon_Death>

		<Strafe_Distance>400.0</Strafe_Distance>
		<Population_Value>0</Population_Value>
		<Required_Planets>locked</Required_Planets>
	</EE_SpaceUnit>

	<EE_Squadron Name="Hutts_Squad">
		<Required_Planets>locked</Required_Planets>
		<Text_ID>TEXT_DALEK_SOLDIER</Text_ID>
		<Encyclopedia_Good_Against> SW_TIE_Interceptor FV_Type9 Starviper_Fighter </Encyclopedia_Good_Against>
		<Encyclopedia_Vulnerable_To> SW_Corellian_Corvette FV_Constitution Firestar </Encyclopedia_Vulnerable_To>
		<Encyclopedia_Text>TEXT_TOOLTIP_DALEK_SOLDIER</Encyclopedia_Text>
		<Encyclopedia_Unit_Class>TEXT_ENCYCLOPEDIA_CLASS_FIGHTER</Encyclopedia_Unit_Class>
		<GUI_Row> 1 </GUI_Row>
		<Is_Dummy>Yes</Is_Dummy>
		<Formation_Priority>1</Formation_Priority>
		<Is_Escort>Yes</Is_Escort>
		<Is_Bomber>no</Is_Bomber>
		<Autoresolve_Health>300</Autoresolve_Health>
		<Damage>10</Damage>
		<Affiliation>B5</Affiliation>
		<Build_Cost_Credits>300</Build_Cost_Credits>
		<Build_Time_Seconds>10</Build_Time_Seconds>
		<Build_Tab_Space_Units>Yes</Build_Tab_Space_Units>
		<Build_Initially_Locked>No</Build_Initially_Locked>
		<Build_Can_Be_Unlocked_By_Slicer>No</Build_Can_Be_Unlocked_By_Slicer>
		<Tech_Level>0</Tech_Level>		
                <Required_Timeline />
		<Required_Ground_Base_Level>0</Required_Ground_Base_Level>
		<Required_Star_Base_Level>1</Required_Star_Base_Level>
		<Behavior>DUMMY_SPACE_FIGHTER_SQUADRON</Behavior>
		<Squadron_Units>Hutts, Hutts, Hutts</Squadron_Units>
		<Max_Squad_Size> 3 </Max_Squad_Size>	
		<Squadron_Offsets>50.0,0.0,0.0</Squadron_Offsets>
		<Squadron_Offsets>0.0,25.0,0.0</Squadron_Offsets>
		<Squadron_Offsets>0.0,-25.0,0.0</Squadron_Offsets>

		<Icon_Name>I_BUTTON_dalek_soldier.tga</Icon_Name>
		<GUI_Model_Name>EV_TieFighter.ALO</GUI_Model_Name>
		<GUI_Distance>120</GUI_Distance>
		<GUI_Offset>0 0 0</GUI_Offset>
		<GUI_Velocity>45</GUI_Velocity>

		<SFXEvent_Build_Started>EHD_Build_Vehicle</SFXEvent_Build_Started>
		<SFXEvent_Build_Cancelled>EHD_Unit_Canceled</SFXEvent_Build_Cancelled>
		<SFXEvent_Build_Complete>Unit_Complete_Todesgleiter</SFXEvent_Build_Complete>

		<FormationOrder>1</FormationOrder>
		<Squadron_Formation_Error_Tolerance>35.0</Squadron_Formation_Error_Tolerance>
		<Guard_Chase_Range>1000.0</Guard_Chase_Range>
		<Idle_Chase_Range>200.0</Idle_Chase_Range>
		<Attack_Move_Response_Range>300.0</Attack_Move_Response_Range>
		<Autonomous_Move_Extension_Vs_Attacker>500.0</Autonomous_Move_Extension_Vs_Attacker>
		<Property_Flags> SmallShip </Property_Flags>
		<Score_Cost_Credits> 420</Score_Cost_Credits>
		
		<!-- Non-Hero unit abilities description -->
		<Unit_Abilities_Data SubObjectList="Yes">
			<!-- Primary ability -->
			<Unit_Ability>
				<Type>HUNT</Type>
			</Unit_Ability>
		</Unit_Abilities_Data>
			
		
		<!--<MULTIPLAYER SKIRMISH VALUES BEGIN>-->
		<Tactical_Build_Cost_Multiplayer>600</Tactical_Build_Cost_Multiplayer>
		<Tactical_Build_Time_Seconds>15</Tactical_Build_Time_Seconds>
		<Tactical_Build_Prerequisites />
		<Tactical_Production_Queue>Tactical_Units</Tactical_Production_Queue>
		<!--<Build_Limit_Current_Per_Player>0</Build_Limit_Current_Per_Player>-->
		<!--<Build_Limit_Lifetime_Per_Player>0</Build_Limit_Lifetime_Per_Player>-->  
		<!--<MULTIPLAYER SKIRMISH VALUES END>-->
		<Population_Value>1</Population_Value>
		<Required_Planets>locked</Required_Planets>
	</EE_Squadron>








<!-- Dalek Mothership -->
	<EE_SpaceUnit Name="Dalek_Saucer">
		<Required_Planets>locked</Required_Planets>
		<Text_ID>TEXT_UNIT_DALEK_SAUCER</Text_ID>
		<Encyclopedia_Good_Against> SW_Executor_SSD BV_Cube New_Basestar </Encyclopedia_Good_Against>
		<Encyclopedia_Vulnerable_To> TIE_Bomber FV_Danube_Torpedo Z95_Headhunter_Underworld </Encyclopedia_Vulnerable_To>
		<Encyclopedia_Text> TEXT_TOOLTIP_DALEK_SAUCER</Encyclopedia_Text>
		<GUI_Row> 1 </GUI_Row>
		<Space_Model_Name>Huttsaucer.ALO</Space_Model_Name>
		<Select_Box_Scale>570</Select_Box_Scale>
		<Select_Box_Z_Adjust>-50</Select_Box_Z_Adjust>
		<Mass>0.995</Mass>
		<Scale_Factor>1.3</Scale_Factor>
		<Damage>150</Damage>
		<Dense_FOW_Reveal_Range_Multiplier>0.24</Dense_FOW_Reveal_Range_Multiplier>
		<Visible_On_Radar_When_Fogged>true</Visible_On_Radar_When_Fogged>
		<Multisample_FOW_Check>Yes</Multisample_FOW_Check>
		<Ranking_In_Category>5</Ranking_In_Category>
		<Max_Speed>1.6</Max_Speed>
		<Max_Rate_Of_Turn>0.8</Max_Rate_Of_Turn>
		<Hyperspace>Yes</Hyperspace>
		<Hyperspace_Speed>1</Hyperspace_Speed>
		<Maintenance_Cost>0.3</Maintenance_Cost>
		<MovementClass> Space </MovementClass>
		<Space_Layer> Capital </Space_Layer>
		<Layer_Z_Adjust>-50.0</Layer_Z_Adjust>
		<OverrideAcceleration> .04 </OverrideAcceleration>
		<OverrideDeceleration> .04 </OverrideDeceleration>
		<Armor_Type> Armor_set5 </Armor_Type>
		<Shield_Armor_Type>Shield_set5</Shield_Armor_Type>
		<Death_Clone>Damage_Normal, SG1_Large_Explosion_Space</Death_Clone>
		<Damage>150</Damage>
		<Autoresolve_Health>99999999999999999</Autoresolve_Health>
		<Shield_Points>10000</Shield_Points>
		<Tactical_Health>10000</Tactical_Health>
		<Shield_Refresh_Rate>1000</Shield_Refresh_Rate>
		<Energy_Capacity>15000</Energy_Capacity>
		<Energy_Refresh_Rate>1000</Energy_Refresh_Rate>
		<Affiliation>B5</Affiliation>
		<Ship_Class>capital</Ship_Class>
		<Tech_Level>5</Tech_Level>
		<Required_Timeline>0</Required_Timeline>
		<Required_Ground_Base_Level>0</Required_Ground_Base_Level>
		<Required_Star_Base_Level>5</Required_Star_Base_Level>
		<Formation_Priority>5</Formation_Priority>
		<Is_Bomber>no</Is_Bomber>
		<Surface_Bombardment_Capable>yes</Surface_Bombardment_Capable>
		<!--<Political_Control>1</Political_Control>-->
		<Political_Faction>Rebel</Political_Faction>
		<Squadron_Capacity>50</Squadron_Capacity>
		<Transport_Capacity>30</Transport_Capacity>
		<Number_per_Squadron>1</Number_per_Squadron>
		<AI_Combat_Power>6000</AI_Combat_Power>
		<Size_Value>360</Size_Value>
		<Behavior> SELECTABLE, DUMMY_STARSHIP </Behavior>
		<SpaceBehavior>ABILITY_COUNTDOWN, SIMPLE_SPACE_LOCOMOTOR, SELECTABLE, TARGETING, WEAPON, REVEAL, HIDE_WHEN_FOGGED, UNIT_AI, DAMAGE_TRACKING, ION_STUN_EFFECT, NEBULA, SPAWN_SQUADRON  </SpaceBehavior>
		<Starting_Spawned_Units_Tech_0>Hutts_Squad, 1</Starting_Spawned_Units_Tech_0>
		<Spawned_Squadron_Delay_Seconds>3</Spawned_Squadron_Delay_Seconds>
		<Reserve_Spawned_Units_Tech_0>Hutts_Squad, -1</Reserve_Spawned_Units_Tech_0>
		<Collidable_By_Projectile_Living>Yes</Collidable_By_Projectile_Living>
		<Space_FOW_Reveal_Range>2400.0</Space_FOW_Reveal_Range>
		<Dense_FOW_Reveal_Range_Multiplier>0.2</Dense_FOW_Reveal_Range_Multiplier>
		<Targeting_Max_Attack_Distance>2200.0</Targeting_Max_Attack_Distance>
		<HardPoints>
			Dalek_Saucer_object6,
			Dalek_Saucer_Hangar,
		</HardPoints>
		<Icon_Name>I_BUTTON_dalek_saucer.tga</Icon_Name>
		<GUI_Model_Name>Antiker.ALO</GUI_Model_Name>
		<GUI_Distance>650</GUI_Distance>
		<GUI_Offset>0 0 0</GUI_Offset>
		<GUI_Velocity>45</GUI_Velocity>
		<Victory_Relevant>yes</Victory_Relevant>
		<Has_Space_Evaluator>True</Has_Space_Evaluator>
		<SFXEvent_Build_Started>RHD_Build_Vehicle</SFXEvent_Build_Started>
		<SFXEvent_Build_Cancelled>RHD_Unit_Canceled</SFXEvent_Build_Cancelled>
		<SFXEvent_Build_Complete>Unit_Complete_Aurora</SFXEvent_Build_Complete>
		<SFXEvent_Ambient_Loop>Unit_Aurora_Moving_Engine_Loop</SFXEvent_Ambient_Loop>
		<SFXEvent_Select>Unit_Select_Aurora</SFXEvent_Select>
		<SFXEvent_Move>Unit_Move_Aurora</SFXEvent_Move>
		<SFXEvent_Fleet_Move>Unit_Fleet_Move_Aurora</SFXEvent_Fleet_Move>
		<SFXEvent_Attack>Unit_Attack_Aurora</SFXEvent_Attack>
		<SFXEvent_Guard>Unit_Guard_Aurora</SFXEvent_Guard>
		<SFXEvent_Stop>Unit_Stop_Aurora</SFXEvent_Stop>
		<SFXEvent_Barrage>Unit_Barrage_Aurora</SFXEvent_Barrage>
		<SFXEvent_Move_Into_Asteroid_Field> Unit_Asteroids_Aurora </SFXEvent_Move_Into_Asteroid_Field>
		<SFXEvent_Move_Into_Nebula> Unit_Nebula_Aurora </SFXEvent_Move_Into_Nebula>
		<SFXEvent_Damaged_By_Asteroid> SFX_Asteroid_Detonation </SFXEvent_Damaged_By_Asteroid>
		<SFXEvent_Attack_Hardpoint> HARD_POINT_WEAPON_LASER, Unit_HP_LASER_Aurora </SFXEvent_Attack_Hardpoint>
		<SFXEvent_Attack_Hardpoint> HARD_POINT_WEAPON_MISSILE, Unit_HP_MISSILE_Aurora </SFXEvent_Attack_Hardpoint>
		<SFXEvent_Attack_Hardpoint> HARD_POINT_WEAPON_TORPEDO, Unit_HP_TORP_Aurora </SFXEvent_Attack_Hardpoint>
		<SFXEvent_Attack_Hardpoint> HARD_POINT_WEAPON_ION_CANNON, Unit_HP_ION_Aurora </SFXEvent_Attack_Hardpoint>
		<SFXEvent_Attack_Hardpoint> HARD_POINT_SHIELD_GENERATOR, Unit_HP_SHIELDS_Aurora </SFXEvent_Attack_Hardpoint>
		<SFXEvent_Attack_Hardpoint> HARD_POINT_ENGINE, Unit_HP_ENGINES_Aurora </SFXEvent_Attack_Hardpoint>
		<SFXEvent_Hardpoint_Destroyed> HARD_POINT_WEAPON_LASER, Unit_Lost_Laser_Aurora </SFXEvent_Hardpoint_Destroyed>
		<SFXEvent_Hardpoint_Destroyed> HARD_POINT_WEAPON_MISSILE,  </SFXEvent_Hardpoint_Destroyed>
		<SFXEvent_Hardpoint_Destroyed> HARD_POINT_WEAPON_TORPEDO,  </SFXEvent_Hardpoint_Destroyed>
		<SFXEvent_Hardpoint_Destroyed> HARD_POINT_WEAPON_ION_CANNON, Unit_Lost_Ion_Aurora </SFXEvent_Hardpoint_Destroyed>
		<SFXEvent_Hardpoint_Destroyed> HARD_POINT_SHIELD_GENERATOR, Unit_Lost_Shields_Aurora </SFXEvent_Hardpoint_Destroyed>
		<SFXEvent_Hardpoint_Destroyed> HARD_POINT_ENGINE, Unit_Lost_Engines_Aurora </SFXEvent_Hardpoint_Destroyed>
		<SFXEvent_Hardpoint_Destroyed> HARD_POINT_GRAVITY_WELL,  </SFXEvent_Hardpoint_Destroyed>
		<SFXEvent_Hardpoint_Destroyed> HARD_POINT_FIGHTER_BAY, </SFXEvent_Hardpoint_Destroyed>
		<SFXEvent_Hardpoint_Destroyed> HARD_POINT_TRACTOR_BEAM, </SFXEvent_Hardpoint_Destroyed>
		<SFXEvent_Hardpoint_Destroyed> HARD_POINT_ENABLE_SPECIAL_ABILITY, </SFXEvent_Hardpoint_Destroyed>
		<SFXEvent_Engine_Idle_Loop> Unit_Aurora_Idle_Engine_Loop </SFXEvent_Engine_Idle_Loop>
		<SFXEvent_Engine_Moving_Loop> Unit_Aurora_Moving_Engine_Loop </SFXEvent_Engine_Moving_Loop>
		<SFXEvent_Engine_Cinematic_Focus_Loop> Unit_Aurora_Cinematic_Engine_Loop </SFXEvent_Engine_Cinematic_Focus_Loop>
		<CategoryMask> Capital | AntiFrigate </CategoryMask>
		<GUI_Model_Name>antiker.ALO</GUI_Model_Name>
		<GUI_Distance>650</GUI_Distance>
		<GUI_Offset>0 0 0</GUI_Offset>
		<GUI_Velocity>45</GUI_Velocity>
		<Victory_Relevant>yes</Victory_Relevant>
		<Space_Full_Stop_Command> Yes </Space_Full_Stop_Command>
		<Has_Space_Evaluator>True</Has_Space_Evaluator>
		<Guard_Chase_Range>1000.0</Guard_Chase_Range>
		<Idle_Chase_Range>0.0</Idle_Chase_Range>
		<Attack_Move_Response_Range>400.0</Attack_Move_Response_Range>
		<Targeting_Stickiness_Time_Threshold>5.0</Targeting_Stickiness_Time_Threshold>
		<Targeting_Priority_Set>Capital</Targeting_Priority_Set>
		<Is_Visible_On_Radar>Yes</Is_Visible_On_Radar>
		<Radar_Icon_Name>i_radar_capital_ship_empire.tga</Radar_Icon_Name>
		<Radar_Icon_Scale_Land>1.0</Radar_Icon_Scale_Land>
		<Radar_Icon_Scale_Space>1.0</Radar_Icon_Scale_Space>

		<Encyclopedia_Unit_Class> TEXT_ENCYCLOPEDIA_CLASS_CAPITAL </Encyclopedia_Unit_Class>
		<Score_Cost_Credits> 45000</Score_Cost_Credits>
		<AI_Combat_Power>10000</AI_Combat_Power>
		<Score_Cost_Credits> 50000 </Score_Cost_Credits>
		<Build_Cost_Credits>10000</Build_Cost_Credits>
		<Build_Time_Seconds>120</Build_Time_Seconds>
		<Build_Tab_Space_Units>Yes</Build_Tab_Space_Units>
		<CategoryMask> Capital | SpaceHero | AntiCapital </CategoryMask>
		<Population_Value>1</Population_Value>
		<!--<MULTIPLAYER SKIRMISH VALUES BEGIN>-->
		<Tactical_Build_Cost_Multiplayer>1000</Tactical_Build_Cost_Multiplayer>
		<Tactical_Build_Time_Seconds>10</Tactical_Build_Time_Seconds>
		<Tactical_Build_Prerequisites />
		<Tactical_Production_Queue>Tactical_Units</Tactical_Production_Queue>
		<Build_Limit_Current_Per_Player>1</Build_Limit_Current_Per_Player>
		<Build_Limit_Lifetime_For_All_Allies>3</Build_Limit_Lifetime_For_All_Allies>
		<Unit_Abilities_Data SubObjectList="Yes">
			<!-- Primary ability -->

			<Unit_Ability>
				<Type>FULL_SALVO</Type>
             <Alternate_Icon_Name>SG_ABILITY_DRONE.TGA</Alternate_Icon_Name>
             <Alternate_Name_Text>TEXT_TOOLTIP_DRONES</Alternate_Name_Text>
                <Alternate_Description_Text>TEXT_TOOLTIP_DRONES_DETAILS</Alternate_Description_Text>

				<Mod_Multiplier>SHIELD_REGEN_MULTIPLIER,	-100.0f</Mod_Multiplier>
				<Mod_Multiplier>SHIELD_REGEN_INTERVAL_MULTIPLIER,	0.01f</Mod_Multiplier>
                <Recharge_Seconds>60</Recharge_Seconds>
                <Expiration_Seconds>15</Expiration_Seconds>
				<SFXEvent_GUI_Unit_Ability_Activated>Unit_Barrage_Interceptor_IV</SFXEvent_GUI_Unit_Ability_Activated>
                <GUI_Activated_Ability_Name>Interceptor_Full_Salvo</GUI_Activated_Ability_Name> 
                <Must_Be_Bought_On_Black_Market>No</Must_Be_Bought_On_Black_Market>
            </Unit_Ability>
		</Unit_Abilities_Data>

		<!--<MULTIPLAYER SKIRMISH VALUES END>-->
		<!-- Hero abilities for Captain Piett added to ship; Hero "moveable" unit removed -->
		<Abilities SubObjectList="Yes">
			<Combat_Bonus_Ability Name="Piet_Combat_Bonus_General">
				<Activation_Style>Space_Automatic</Activation_Style>
				<Unit_Strength_Category>Capital | Corvette | Frigate | Fighter</Unit_Strength_Category>
				<Applicable_Unit_Categories>Capital | Corvette | Frigate | Fighter</Applicable_Unit_Categories>
				<Applicable_Unit_Types/>
				<Health_Bonus_Percentage>0.25</Health_Bonus_Percentage>
				<Damage_Bonus_Percentage>0.0</Damage_Bonus_Percentage>
				<Energy_Pool_Bonus_Percentage>0.0</Energy_Pool_Bonus_Percentage>
				<Shield_Bonus_Percentage>0.1</Shield_Bonus_Percentage>
				<Defense_Bonus_Percentage>0.0</Defense_Bonus_Percentage>
				<Movement_Speed_Bonus_Percentage>0.0</Movement_Speed_Bonus_Percentage>
				<Stacking_Category>0</Stacking_Category>
			</Combat_Bonus_Ability>
			<Combat_Bonus_Ability Name="Piet_Combat_Bonus_Star_Destroyer">
				<Unit_Strength_Category/>
				<Activation_Style>Space_Automatic</Activation_Style>
				<Applicable_Unit_Categories/>
				<Applicable_Unit_Types>Star_Destroyer</Applicable_Unit_Types>
				<Health_Bonus_Percentage>0.25</Health_Bonus_Percentage>
				<Damage_Bonus_Percentage>0.0</Damage_Bonus_Percentage>
				<Energy_Pool_Bonus_Percentage>0.0</Energy_Pool_Bonus_Percentage>
				<Shield_Bonus_Percentage>0.0</Shield_Bonus_Percentage>
				<Defense_Bonus_Percentage>0.0</Defense_Bonus_Percentage>
				<Movement_Speed_Bonus_Percentage>0.0</Movement_Speed_Bonus_Percentage>
				<!-- note stacking category, this stacks with other hero combat bonuses -->
				<Stacking_Category>1</Stacking_Category>
			</Combat_Bonus_Ability>
		</Abilities>
		<Tactical_Bribe_Cost>6100</Tactical_Bribe_Cost>
		<Is_Visible_On_Radar>Yes</Is_Visible_On_Radar>
		<Radar_Icon_Size>0.074  0.074</Radar_Icon_Size>
    		<Radar_Icon_Name>I_RADAR_AURORA.tga</Radar_Icon_Name>
		<Radar_Icon_Scale_Land>1.0</Radar_Icon_Scale_Land>
		<Radar_Icon_Scale_Space>1.5</Radar_Icon_Scale_Space>
		<Required_Planets>locked</Required_Planets>
	</EE_SpaceUnit>
	
	
		<HardPoint Name="Dalek_Saucer_object6">
		<Type> HARD_POINT_WEAPON_LASER </Type>
		<Is_Targetable>No</Is_Targetable>
		<Is_Destroyable>Yes</Is_Destroyable>
		<Tooltip_Text>TEXT_WEAPON_TURBOLASER1</Tooltip_Text>
		<Health>46.0</Health>
		<Death_Explosion_Particles> SG1_Large_Explosion_Space </Death_Explosion_Particles>
		<Death_Explosion_SFXEvent>Unit_Hardpoint_Laser_Death</Death_Explosion_SFXEvent>
		<Attachment_Bone>object6</Attachment_Bone>
		<Collision_Mesh>object6</Collision_Mesh>
		<Damage_Decal>HP_B-R_BLAST</Damage_Decal>
		<Damage_Particles>HP_B-R_EMITDAMAGE</Damage_Particles>
		
		<Fire_Bone_A>object6</Fire_Bone_A>
		<Fire_Bone_B>object6</Fire_Bone_B>
		<Fire_Cone_Width>360.0</Fire_Cone_Width>
		<Fire_Cone_Height>180.0</Fire_Cone_Height>
		<Fire_Projectile_Type>Proj_Dalek_MS_Green</Fire_Projectile_Type>
		<Fire_Min_Recharge_Seconds>1.0</Fire_Min_Recharge_Seconds>
		<Fire_Max_Recharge_Seconds>5.0</Fire_Max_Recharge_Seconds>
		<Fire_Pulse_Count>5</Fire_Pulse_Count>
		<Fire_Pulse_Delay_Seconds>0.09</Fire_Pulse_Delay_Seconds>
		<Fire_Range_Distance>1400.0</Fire_Range_Distance>
		<Fire_SFXEvent>Unit_ID4_beam</Fire_SFXEvent>
		<Fire_Inaccuracy_Distance> Fighter, 1.0 </Fire_Inaccuracy_Distance>
		<Fire_Inaccuracy_Distance> Bomber, 1.0 </Fire_Inaccuracy_Distance>
		<Fire_Inaccuracy_Distance> Transport, 1.0 </Fire_Inaccuracy_Distance>
		<Fire_Inaccuracy_Distance> Corvette, 1.0 </Fire_Inaccuracy_Distance>
		<Fire_Inaccuracy_Distance> Frigate, 1.0 </Fire_Inaccuracy_Distance>
		<Fire_Inaccuracy_Distance> Capital, 1.0 </Fire_Inaccuracy_Distance>
		<Fire_Inaccuracy_Distance> Super, 1.0 </Fire_Inaccuracy_Distance>
	</HardPoint>




	<HardPoint Name="Dalek_Saucer_Hangar">
		<Type> HARD_POINT_FIGHTER_BAY </Type>
		<Is_Targetable>No</Is_Targetable>
		<Is_Destroyable>No</Is_Destroyable>
		<Attachment_Bone>object6</Attachment_Bone>
		<Collision_Mesh>object6</Collision_Mesh>
		<Tooltip_Text>TEXT_FIGHTER_BAY_HARDPOINT</Tooltip_Text>
		<Health>10.0</Health>
		<Death_Explosion_Particles> SG1_Large_Explosion_Space_Empire </Death_Explosion_Particles>
		<!-- STUBBED PLACEHOLDER -->
		<Death_Explosion_SFXEvent>Unit_Hardpoint_Bay_Death</Death_Explosion_SFXEvent>
		<!-- STUBBED PLACEHOLDER -->
	</HardPoint>
	
	<HardPoint Name="Dalek_Soldier">
		<Type> HARD_POINT_WEAPON_LASER </Type>
		<Is_Targetable>No</Is_Targetable>
		<Is_Destroyable>No</Is_Destroyable>
		<Tooltip_Text>TEXT_WEAPON_TURBOLASER1</Tooltip_Text>
		<Health>46.0</Health>
		<Death_Explosion_Particles> SG1_Large_Explosion_Space </Death_Explosion_Particles>
		<Death_Explosion_SFXEvent>Unit_Hardpoint_Laser_Death</Death_Explosion_SFXEvent>
		<Attachment_Bone>dalek001_0</Attachment_Bone>
		<Collision_Mesh>dalek001_0</Collision_Mesh>
		<Damage_Decal>HP_B-R_BLAST</Damage_Decal>
		<Damage_Particles>HP_B-R_EMITDAMAGE</Damage_Particles>
		
		<Fire_Bone_A>dalek001_0</Fire_Bone_A>
		<Fire_Bone_B>dalek001_0</Fire_Bone_B>
		<Fire_Cone_Width>180.0</Fire_Cone_Width>
		<Fire_Cone_Height>170.0</Fire_Cone_Height>
		<Fire_Projectile_Type>Proj_Dalek_Laser_Green</Fire_Projectile_Type>
		<Fire_Min_Recharge_Seconds>1.0</Fire_Min_Recharge_Seconds>
		<Fire_Max_Recharge_Seconds>5.0</Fire_Max_Recharge_Seconds>
		<Fire_Pulse_Count>5</Fire_Pulse_Count>
		<Fire_Pulse_Delay_Seconds>0.09</Fire_Pulse_Delay_Seconds>
		<Fire_Range_Distance>1400.0</Fire_Range_Distance>
		<Fire_SFXEvent>Unit_304_Laser_Fire</Fire_SFXEvent>
		<Fire_Inaccuracy_Distance> Fighter, 1.0 </Fire_Inaccuracy_Distance>
		<Fire_Inaccuracy_Distance> Bomber, 1.0 </Fire_Inaccuracy_Distance>
		<Fire_Inaccuracy_Distance> Transport, 1.0 </Fire_Inaccuracy_Distance>
		<Fire_Inaccuracy_Distance> Corvette, 1.0 </Fire_Inaccuracy_Distance>
		<Fire_Inaccuracy_Distance> Frigate, 1.0 </Fire_Inaccuracy_Distance>
		<Fire_Inaccuracy_Distance> Capital, 1.0 </Fire_Inaccuracy_Distance>
		<Fire_Inaccuracy_Distance> Super, 1.0 </Fire_Inaccuracy_Distance>
	</HardPoint>
	
	<Projectile Name="Proj_Dalek_Laser_Green">
		<Text_ID>TEXT_NONE</Text_ID>
		<Space_Model_Name>W_LASER_LARGEG.ALO</Space_Model_Name>
		<Projectile_Custom_Render>1</Projectile_Custom_Render>
		<Projectile_Width>0.1</Projectile_Width>
		<Projectile_Length>6.0</Projectile_Length>
		<Projectile_Texture_Slot>3,0</Projectile_Texture_Slot>
		<Scale_Factor>1.0</Scale_Factor>
		<Max_Speed>70.0</Max_Speed>
		<Max_Rate_Of_Turn>0.0</Max_Rate_Of_Turn>
		<Damage_Type> Damage_light_set1 </Damage_Type>
		<Behavior>PROJECTILE, HIDE_WHEN_FOGGED</Behavior>
		<Projectile_Category>Laser</Projectile_Category>
		<Projectile_Max_Flight_Distance>2200.0</Projectile_Max_Flight_Distance>
		<Projectile_Damage>20.0</Projectile_Damage>
		<Projectile_Does_Shield_Damage>Yes</Projectile_Does_Shield_Damage>
		<Projectile_Does_Energy_Damage>No</Projectile_Does_Energy_Damage>
		<Projectile_Does_Hitpoint_Damage>Yes</Projectile_Does_Hitpoint_Damage>
		<Projectile_Energy_Per_Shot>20</Projectile_Energy_Per_Shot>
		<Projectile_Object_Armor_Reduced_Detonation_Particle>Large_Dud</Projectile_Object_Armor_Reduced_Detonation_Particle>
		<Projectile_Object_Detonation_Particle>Small_Explosion_Space</Projectile_Object_Detonation_Particle>
		<Projectile_Ground_Detonation_Particle>Small_Explosion_Space</Projectile_Ground_Detonation_Particle>
		<Projectile_Lifetime_Detonation_Particle>Small_Explosion_Space</Projectile_Lifetime_Detonation_Particle>
		<Projectile_Absorbed_By_Shields_Particle>Projectile_Shield_Absorb_Large</Projectile_Absorbed_By_Shields_Particle>
		<AI_Combat_Power>500</AI_Combat_Power>
		<Projectile_SFXEvent_Detonate> SFX_Small_Damage_Detonation </Projectile_SFXEvent_Detonate>
		<Projectile_SFXEvent_Detonate_Reduced_By_Armor> SFX_Small_Damage_Detonation </Projectile_SFXEvent_Detonate_Reduced_By_Armor>
	</Projectile>



<!-- Dalek Mothership -->


	<Projectile Name="Proj_Dalek_MS_Green">
		<Text_ID>TEXT_NONE</Text_ID>
		<Space_Model_Name>W_LASER_LARGEG.ALO</Space_Model_Name>
		<Projectile_Custom_Render>1</Projectile_Custom_Render>
		<Projectile_Width>1.0</Projectile_Width>
		<Projectile_Length>60.0</Projectile_Length>
		<Projectile_Texture_Slot>3,0</Projectile_Texture_Slot>
		<Scale_Factor>1.0</Scale_Factor>
		<Max_Speed>70.0</Max_Speed>
		<Max_Rate_Of_Turn>0.0</Max_Rate_Of_Turn>
		<Damage_Type> Damage_set6 </Damage_Type>
		<Behavior>PROJECTILE, HIDE_WHEN_FOGGED</Behavior>
		<Projectile_Category>Laser</Projectile_Category>
		<Projectile_Max_Flight_Distance>2200.0</Projectile_Max_Flight_Distance>
		<Projectile_Damage>200.0</Projectile_Damage>
		<Projectile_Does_Shield_Damage>Yes</Projectile_Does_Shield_Damage>
		<Projectile_Does_Energy_Damage>No</Projectile_Does_Energy_Damage>
		<Projectile_Does_Hitpoint_Damage>Yes</Projectile_Does_Hitpoint_Damage>
		<Projectile_Energy_Per_Shot>20</Projectile_Energy_Per_Shot>
		<Projectile_Object_Armor_Reduced_Detonation_Particle>Large_Dud</Projectile_Object_Armor_Reduced_Detonation_Particle>
		<Projectile_Object_Detonation_Particle>Small_Explosion_Space</Projectile_Object_Detonation_Particle>
		<Projectile_Ground_Detonation_Particle>Small_Explosion_Space</Projectile_Ground_Detonation_Particle>
		<Projectile_Lifetime_Detonation_Particle>Small_Explosion_Space</Projectile_Lifetime_Detonation_Particle>
		<Projectile_Absorbed_By_Shields_Particle>Projectile_Shield_Absorb_Large</Projectile_Absorbed_By_Shields_Particle>
		<AI_Combat_Power>500</AI_Combat_Power>
		<Projectile_SFXEvent_Detonate> SFX_Small_Damage_Detonation </Projectile_SFXEvent_Detonate>
		<Projectile_SFXEvent_Detonate_Reduced_By_Armor> SFX_Small_Damage_Detonation </Projectile_SFXEvent_Detonate_Reduced_By_Armor>
	</Projectile>